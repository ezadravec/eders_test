function openModal(id) {
	$.ajax({
		url: '/repository/details/' + id,
		method: 'GET',
		success: function(result) {
			$('#details-modal .title').html(result.name);

			$('#details-modal .description').html(result.description)
			$('#details-modal .homepage').attr('href', result.html_url)
			$('#details-modal .branch').html(result.default_branch)
			$('#details-modal .language').html(result.language)
			$('#details-modal .create').html(result.created_at)
			$('#details-modal .update').html(result.updated_at)
			$('#details-modal .clone-ssh').html(result.clone_url)
			

			$("#details-modal").modal('show');
		}
	});
}