<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use \Github\Client as Github;

class GitController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $client = new Github();

        $repositories = $client->api('user')->repositories('symfony');

        $projects = [];
        foreach ($repositories as $v) {
            $projects[] = [
                'name'          => $v['name'],
                'description'   => $v['description'],
            ];
        }

        unset($repositories);

        return $this->render('default/index.html.twig', ['projects' => $projects]);
    }

    /**
     * @Route("/repository/details/{id}", name="details")
     */
    public function detailsAction($id)
    {
        $client = new Github();

        $repository = $client->api('repo')->show('symfony', $id);

        $response = new JsonResponse($repository);

        return $response;
    }
}
